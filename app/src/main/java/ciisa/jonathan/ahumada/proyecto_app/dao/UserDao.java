package ciisa.jonathan.ahumada.proyecto_app.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import ciisa.jonathan.ahumada.proyecto_app.models.UserEntity;

@Dao
public interface UserDao {
    @Query("Select  id,  username,  firstName,  lastName,  birthday,  height,  password from user where username = :username Limit 1")
    UserEntity findByUserName(String username);

    @Insert
    long insert(UserEntity user);
}
