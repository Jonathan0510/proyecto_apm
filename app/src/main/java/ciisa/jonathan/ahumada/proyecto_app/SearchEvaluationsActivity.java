package ciisa.jonathan.ahumada.proyecto_app;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ciisa.jonathan.ahumada.proyecto_app.controller.EvaluationController;
import ciisa.jonathan.ahumada.proyecto_app.models.Evaluation;
import ciisa.jonathan.ahumada.proyecto_app.ui.DatePickerFragment;
import ciisa.jonathan.ahumada.proyecto_app.ui.EvaluationAdapter;

public class SearchEvaluationsActivity extends AppCompatActivity {

    private Button btnReturn, btnSearch, btnclear;
    private TextInputLayout tilDateInitial, tilDateFinal;
    private final String DATE_PATTERN = "yyyy-MM-dd";
    private SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
    private ListView lvEvaluationsList;
    private EvaluationController evaluationController;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_evaluations);

        evaluationController = new EvaluationController(this);

        tilDateInitial = findViewById(R.id.activity_search_field_initial_date);
        tilDateFinal = findViewById(R.id.activity_search_field_final_date);
        btnSearch = findViewById(R.id.activity_search_evaluation_btn_search);
        btnReturn = findViewById(R.id.activity_search_evaluation_btn_return);
        btnclear = findViewById(R.id.activity_search_evaluation_btn_clear);
        lvEvaluationsList = findViewById(R.id.activity_evaluation_detail_lv_evaluations);



        List<Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter = new EvaluationAdapter(this,evaluationList);
        lvEvaluationsList.setAdapter(adapter);

        lvEvaluationsList.setOnItemClickListener(((adapterView, view, index, id) -> {
            Evaluation evaluation = evaluationList.get(index);

            Intent i = new Intent(view.getContext(), EvaluationDetailsActivity.class);
            i.putExtra("evaluation", evaluation);
            view.getContext().startActivity(i);
        }));


        tilDateInitial.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDateInitial, new Date());
        });


        tilDateFinal.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDateFinal, new Date());
        });


        btnSearch.setOnClickListener(view -> {
            String fromDate = tilDateInitial.getEditText().getText().toString();
            String toDate = tilDateFinal.getEditText().getText().toString();

            boolean validFromDate = !fromDate.isEmpty();
            boolean validtoDate = !toDate.isEmpty();

            if (validFromDate && validtoDate){
               try {
                    Date from = dateFormatter.parse(fromDate);
                    Date to = dateFormatter.parse(toDate);

                   List<Evaluation> evaluationRangeList = evaluationController.getRange(from, to);
                   EvaluationAdapter rangeAdapter = new EvaluationAdapter(this,evaluationRangeList);
                   lvEvaluationsList.setAdapter(rangeAdapter);

                   lvEvaluationsList.setOnItemClickListener(((adapterView, rangeView, index, id) -> {
                       Evaluation evaluation = evaluationRangeList.get(index);

                       Intent i = new Intent(rangeView.getContext(), EvaluationDetailsActivity.class);
                       i.putExtra("evaluation", evaluation);
                       rangeView.getContext().startActivity(i);
                   }));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

                });

        btnReturn.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),MainActivity.class);
            startActivity(i);
            finish();
        });

        btnclear.setOnClickListener(view -> {
            tilDateInitial.getEditText().setText("");
            tilDateFinal.getEditText().setText("");
            lvEvaluationsList.setAdapter(adapter);

        });


    }
    @Override
    protected void onResume(){
        super.onResume();
        List<Evaluation> evaluationList = evaluationController.getAll();
        EvaluationAdapter adapter = new EvaluationAdapter(this,evaluationList);
        lvEvaluationsList.setAdapter(adapter);

        lvEvaluationsList.setOnItemClickListener(((adapterView, view, index, id) -> {
            Evaluation evaluation = evaluationList.get(index);

            Intent i = new Intent(view.getContext(), EvaluationDetailsActivity.class);
            i.putExtra("evaluation", evaluation);
            view.getContext().startActivity(i);
        }));

    }
}