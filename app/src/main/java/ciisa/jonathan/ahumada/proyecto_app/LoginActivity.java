package ciisa.jonathan.ahumada.proyecto_app;

import android.content.Intent;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;
import ciisa.jonathan.ahumada.proyecto_app.controller.AuthController;
public class LoginActivity extends AppCompatActivity {
    private AuthController authcontroller;
    private Button btnLoginEnter, btnLoginRegister;
    private TextInputLayout tilUser, tilPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authcontroller  = new AuthController(this);

        btnLoginEnter = findViewById(R.id.activity_login_btn_enter);
        btnLoginRegister = findViewById(R.id.activity_login_btn_register);
        tilUser = findViewById(R.id.activity_login_field_username);
        tilPassword = findViewById(R.id.activity_login_field_password);

        btnLoginEnter.setOnClickListener(view -> {
            Toast.makeText(view.getContext(), "Iniciando sesión", Toast.LENGTH_SHORT).show();

            String username = tilUser.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();

            boolean usernamedValid = !password.isEmpty();
            boolean passwordValid = !password.isEmpty();

            if (!passwordValid) {
                tilPassword.setError("Campo requerido");
            } else {
                tilPassword.setError(null);
                tilPassword.setErrorEnabled(false);
            }

            if (!usernamedValid) {
                tilUser.setError("Campo requerido");
            } else {
                tilUser.setError(null);
                tilUser.setErrorEnabled(false);
            }

            if (usernamedValid && passwordValid) {
                authcontroller.login(username, password);
            } else {
                Toast.makeText(view.getContext(), "Campos inválidos", Toast.LENGTH_SHORT).show();
            }
        });

        btnLoginRegister.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),RegisterActivity.class);
            startActivity(i);
            finish();
        });

    }
}