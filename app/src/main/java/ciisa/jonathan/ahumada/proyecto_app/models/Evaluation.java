package ciisa.jonathan.ahumada.proyecto_app.models;

import java.io.Serializable;
import java.util.Date;

public class Evaluation implements Serializable, IEvaluation {

    private long id;
    private Date birthday;
    private double weight;
    private double imc;
    private long userId;

    public Evaluation(Date birthday, double weight, double imc, long userId) {
        this.birthday = birthday;
        this.weight = weight;
        this.imc = imc;
        this.userId =userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getBirthday() {
        return birthday;}

    public double getWeight() {
        return weight;
    }

    public double getImc() {
        return imc;
    }

    public long getUserId(){
        return userId;
    }


}
