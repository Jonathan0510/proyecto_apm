package ciisa.jonathan.ahumada.proyecto_app.models;

public class EvaluationMapper {

    private IEvaluation evaluation;

    public EvaluationMapper (IEvaluation evaluation){this.evaluation = evaluation;}

    public EvaluationEnity toEntity(){
        return new EvaluationEnity(
                this.evaluation.getId(),
                this.evaluation.getBirthday(),
                this.evaluation.getWeight(),
                this.evaluation.getImc(),
                this.evaluation.getUserId()
        );
    }

    public Evaluation toBase(){
        Evaluation evaluation = new Evaluation(
                this.evaluation.getBirthday(),
                this.evaluation.getWeight(),
                this.evaluation.getImc(),
                this.evaluation.getUserId()
        );
        evaluation.setId(this.evaluation.getId());
        return evaluation;
    }
}
