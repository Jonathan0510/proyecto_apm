package ciisa.jonathan.ahumada.proyecto_app.models;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "evaluation")
public class EvaluationEnity implements IEvaluation{

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "birthday")
    private Date birthday;

    @ColumnInfo(name = "weight")
    private double weight;

    @ColumnInfo(name = "imc")
    private double imc;

    @ColumnInfo(name = "user_id")
    private long userId;

    public EvaluationEnity(long id, Date birthday, double weight, double imc, long userId) {
        this.id = id;
        this.birthday = birthday;
        this.weight = weight;
        this.imc = imc;
        this.userId = userId;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public Date getBirthday() {
        return birthday;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public double getImc() {
        return imc;
    }

    @Override
    public long getUserId() {
        return userId;
    }
}
