package ciisa.jonathan.ahumada.proyecto_app;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import ciisa.jonathan.ahumada.proyecto_app.controller.AuthController;

public class MainActivity extends AppCompatActivity {

    private Button btnMainEvaluationlog,btnMainSearchEvaluation,btnMainLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMainEvaluationlog = findViewById(R.id.activity_main_btn_evaluation_log);
        btnMainSearchEvaluation = findViewById(R.id.activity_main_btn_search_evaluations);
        btnMainLogout = findViewById(R.id.activity_main_btn_search_evaluations_logout);

        btnMainEvaluationlog.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(), EvaluationRegisterActivity.class);
            startActivity(i);
            finish();
        });

        btnMainSearchEvaluation.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),SearchEvaluationsActivity.class);
            startActivity(i);
            finish();
        });

        btnMainLogout.setOnClickListener(view -> {
            AuthController controller = new AuthController(this);
            controller.logout();
        });

    }
}