package ciisa.jonathan.ahumada.proyecto_app.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ciisa.jonathan.ahumada.proyecto_app.R;
import ciisa.jonathan.ahumada.proyecto_app.SearchEvaluationsActivity;
import ciisa.jonathan.ahumada.proyecto_app.models.Evaluation;

public class EvaluationAdapter extends BaseAdapter {
    private final String DATE_PATTERN = "yyyy-MM-dd";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
    private Context ctx;
    private List<Evaluation> evaluationList;

    public EvaluationAdapter(Context ctx, List<Evaluation> evaluationList) {
        this.ctx = ctx;
        this.evaluationList = evaluationList;
    }

    @Override
    public int getCount() {

        return evaluationList.size();
    }

    @Override
    public Object getItem(int i) {

        return evaluationList.get(i);
    }

    @Override
    public long getItemId(int i) {

        return evaluationList.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(ctx);

        view = inflater.inflate(R.layout.item_evaluation_list, null);

       Evaluation evaluation =  evaluationList.get(i);


        TextView tvDate = view.findViewById(R.id.item_list_evaluation_field_date_return);
        TextView tvWeigth = view.findViewById(R.id.item_list_evaluation_field_weight_return);
        TextView tvImc = view.findViewById(R.id.item_list_evaluation_field_imc_return);

        String birthdaytoString = dateFormatter.format(evaluation.getBirthday());


        tvDate.setText(birthdaytoString);
        tvWeigth.setText(Long.toString((long) evaluation.getWeight()));
        tvImc.setText(Long.toString((long) evaluation.getImc()));

        return view;
    }
}
