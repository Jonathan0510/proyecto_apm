package ciisa.jonathan.ahumada.proyecto_app.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import ciisa.jonathan.ahumada.proyecto_app.LoginActivity;
import ciisa.jonathan.ahumada.proyecto_app.MainActivity;
import ciisa.jonathan.ahumada.proyecto_app.dao.UserDao;
import ciisa.jonathan.ahumada.proyecto_app.lib.BCrypt;
import ciisa.jonathan.ahumada.proyecto_app.lib.databaseConnectors;
import ciisa.jonathan.ahumada.proyecto_app.models.User;
import ciisa.jonathan.ahumada.proyecto_app.models.UserEntity;
import ciisa.jonathan.ahumada.proyecto_app.models.UserMapper;


public class AuthController {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");


    private final String KEY_USER_ID = "userId";
    private final String KEY_USER = "userName";
    private final String KEY_FIRSTNAME = "userFirstName";
    private final String KEY_LASTNAME = "userlastName";
    private final String KEY_BIRTHDAY = "userBirthday";
    private final String KEY_STATURE = "userStature";

    private UserDao userDao;
    private Context ctx;
    private SharedPreferences preferences;
    private AuthController authController;

    public AuthController(Context ctx) {
        this.ctx = ctx;
        int PRIVATE_MODE = 0;
        String PREF_NAME = "AppRegistroPeso";
        this.preferences = ctx.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.userDao = databaseConnectors.getInstance(ctx).userDao();
    }

    private void setUserSession(User user){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER, user.getUsername());
        editor.putString(KEY_FIRSTNAME, user.getFirstName());
        editor.putString(KEY_LASTNAME, user.getLastName());
        editor.putString(KEY_BIRTHDAY, dateFormat.format(user.getBirthday()));
        editor.putString(KEY_STATURE, String.valueOf(user.getHeight()));
        editor.apply();
    }

    public User getUserSession(){
        long id = preferences.getLong(KEY_USER_ID, 0);
        String username = preferences.getString(KEY_USER,"");
        String firstname = preferences.getString(KEY_FIRSTNAME, "");
        String lastname = preferences.getString(KEY_LASTNAME, "");
        String stature = preferences.getString(KEY_STATURE,"");

        User user = new User (username,firstname,lastname,new Date(),Double.parseDouble(stature));
        user.setId(id);
        return user;
    }

    public void checkUserSession(){
        long id = preferences.getLong(KEY_USER_ID,0);
        authController = new AuthController(ctx);
        User user = authController.getUserSession();

        final int TIMEOUT = 3000;

        new Handler().postDelayed(() -> {
            if (id != 0) {
                Toast.makeText(ctx, String.format("Bienvenido %s", user.getUsername()), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ctx, MainActivity.class);
                ctx.startActivity(i);
            } else {
                Intent i = new Intent(ctx, LoginActivity.class);
                ctx.startActivity(i);
            }
            ((Activity) ctx).finish();
        }, TIMEOUT);
    }

    public void register(User user){
        String hashedPassword = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPassword);

        UserEntity userEntity = new UserMapper(user).toEntity();
        userDao.insert(userEntity);
        Toast.makeText(ctx, String.format("Usuario %s registrado", user.getUsername()), Toast.LENGTH_SHORT).show();
        Intent i = new Intent (ctx, LoginActivity.class);
        ctx.startActivity(i);
    }

    public void login(String username, String password) {

        UserEntity userEntity = userDao.findByUserName(username);


        if (userEntity == null){
            Toast.makeText(ctx, String.format("Credencial Invalida", username), Toast.LENGTH_SHORT).show();
            return;
        }

        User user = new UserMapper(userEntity).toBase();

        if (username.equals(user.getUsername()) && BCrypt.checkpw(password, user.getPassword())) {
            setUserSession(user);
            Toast.makeText(ctx, String.format("Bienvenido %s", username), Toast.LENGTH_SHORT).show();
            Intent i = new Intent(ctx, MainActivity.class);
            ctx.startActivity(i);
            ((Activity) ctx).finish();
        } else {
            Toast.makeText(ctx, String.format("La contraseña o Usuario es incorrecto", username), Toast.LENGTH_SHORT).show();
        }
    }

    public void logout(){
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.clear();
        editor.apply();
        Toast.makeText(ctx, String.format("Cerrando Sesion"), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx,LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
    }

}
