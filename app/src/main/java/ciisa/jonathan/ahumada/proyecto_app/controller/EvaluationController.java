package ciisa.jonathan.ahumada.proyecto_app.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ciisa.jonathan.ahumada.proyecto_app.EvaluationRegisterActivity;
import ciisa.jonathan.ahumada.proyecto_app.dao.EvaluationDao;
import ciisa.jonathan.ahumada.proyecto_app.lib.databaseConnectors;
import ciisa.jonathan.ahumada.proyecto_app.models.Evaluation;
import ciisa.jonathan.ahumada.proyecto_app.models.EvaluationEnity;
import ciisa.jonathan.ahumada.proyecto_app.models.EvaluationMapper;
import ciisa.jonathan.ahumada.proyecto_app.models.User;


public class EvaluationController {

    private EvaluationDao evaluationDao;
    private Context ctx;
    private AuthController authController;


    public EvaluationController(Context ctx) {
        this.ctx = ctx;
        this.evaluationDao = databaseConnectors.getInstance(ctx).evaluationDao();
        this.authController = new AuthController(ctx);
    }

    public void registerEvaluation(Evaluation evaluation) {
        EvaluationEnity evaluationEnity = new EvaluationMapper(evaluation).toEntity();
        evaluationDao.insert(evaluationEnity);
        Toast.makeText(ctx, String.format("Evaluacion registrada"), Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ctx, EvaluationRegisterActivity.class);
        ctx.startActivity(i);
    }

    public void deleteEvaluation(long id) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    try {
                        evaluationDao.delete(id);
                        ((Activity) ctx).onBackPressed();
                        Toast.makeText(ctx, String.format("Evaluacion borrada"), Toast.LENGTH_SHORT).show();
                    } catch (Error error) {
                        error.printStackTrace();
                        Toast.makeText(this.ctx, "Error al eliminar la evaluacion", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this.ctx);
        builder.setMessage("Estás seguro de eliminar la evaluacion?")
                .setPositiveButton("Si", dialogClickListener)
                .setNegativeButton("No", dialogClickListener)
                .show();
    }

    public List<Evaluation> getAll() {
        AuthController authController = new AuthController(ctx);
        User user = authController.getUserSession();
        List<EvaluationEnity> evaluationEntityList = evaluationDao.findAll(user.getId());
        List<Evaluation> evaluationList = new ArrayList<>();

        for (EvaluationEnity evaluationEnity : evaluationEntityList) {

            Evaluation evaluation = new EvaluationMapper(evaluationEnity).toBase();
            evaluationList.add(evaluation);
        }
        return evaluationList;
    }

    public List<Evaluation> getRange(Date from, Date to) {

        User user = authController.getUserSession();
        List<EvaluationEnity> evaluationEntityList = evaluationDao.findByRange(from, to, user.getId());
        List<Evaluation> evaluationList = new ArrayList<>();

        for (EvaluationEnity evaluationEnity : evaluationEntityList) {

            Evaluation evaluation = new EvaluationMapper(evaluationEnity).toBase();
            evaluationList.add(evaluation);
        }
        return evaluationList;
    }

    public double calculateImc(double weight) {
        User user = authController.getUserSession();
        return weight/(user.getHeight() * user.getHeight());
    }


}
