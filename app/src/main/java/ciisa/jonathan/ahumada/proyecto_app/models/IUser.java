package ciisa.jonathan.ahumada.proyecto_app.models;

import java.util.Date;

public interface IUser  {

     long getId();
     String getUsername();
     String getFirstName();
     String getLastName();
     Date getBirthday();
     double getHeight();
     String getPassword();

}
