package ciisa.jonathan.ahumada.proyecto_app.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

import ciisa.jonathan.ahumada.proyecto_app.models.EvaluationEnity;

@Dao
public interface EvaluationDao {
    @Query("Select id,birthday,weight,imc, user_id from evaluation where user_id = :userId")
    List<EvaluationEnity> findAll(long userId);

    @Query("Select id,birthday,weight,imc, user_id from evaluation Where user_id = :userId AND birthday Between :from AND :to")
    List<EvaluationEnity> findByRange(Date from, Date to, long userId);

    @Insert
    long insert(EvaluationEnity evaluation);

    @Query("DELETE FROM evaluation WHERE id = :id")
    void delete (long id);


}
