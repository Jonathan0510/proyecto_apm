package ciisa.jonathan.ahumada.proyecto_app;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import java.text.SimpleDateFormat;
import java.util.Date;

import ciisa.jonathan.ahumada.proyecto_app.controller.AuthController;
import ciisa.jonathan.ahumada.proyecto_app.controller.EvaluationController;
import ciisa.jonathan.ahumada.proyecto_app.models.Evaluation;
import ciisa.jonathan.ahumada.proyecto_app.models.User;
import ciisa.jonathan.ahumada.proyecto_app.ui.DatePickerFragment;
import ciisa.jonathan.ahumada.proyecto_app.utils.DateUtils;


public class EvaluationRegisterActivity extends AppCompatActivity {
    private Button btnReturn, btnEvaluationRegister;
    private TextInputLayout tilDate, tilWeight;
    private AuthController authController;
    private Evaluation evaluation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_register);
        tilDate = findViewById(R.id.activity_evaluation_register_date);
        tilWeight = findViewById(R.id.activity_evaluation_register_weight);

        btnReturn = findViewById(R.id.activity_evaluation_register_btn_return);
        btnEvaluationRegister = findViewById(R.id.activity_evaluation_register_btn_evaluation_register);


        tilDate.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilDate, new Date());
        });


        btnReturn.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),MainActivity.class);
            startActivity(i);
            finish();
        });

        btnEvaluationRegister.setOnClickListener(view -> {
            EvaluationController controller = new EvaluationController(view.getContext());
            authController = new AuthController(view.getContext());

            User user = authController.getUserSession();

            String Date = tilDate.getEditText().getText().toString();
            String weight = tilWeight.getEditText().getText().toString();

            boolean birthdayValid = !Date.isEmpty();

            if (!birthdayValid) {
                tilDate.setError("Campo requerido");
            } else {
                tilDate.setError(null);
                tilDate.setErrorEnabled(false);
            }

            Date stringToDate = DateUtils.unsafeParse(Date);

            double weightDate = 0.0;
            try {
                weightDate = Double.parseDouble(weight);
                tilWeight.setError(null);
                tilWeight.setErrorEnabled(false);
            } catch (NumberFormatException error){
                tilWeight.setError("Peso es invalido");
                return;
            }

            evaluation = new Evaluation(stringToDate,weightDate,controller.calculateImc(weightDate), user.getId());
            controller.registerEvaluation(evaluation);

        });

    }
}