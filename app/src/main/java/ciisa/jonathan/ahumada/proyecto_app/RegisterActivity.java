package ciisa.jonathan.ahumada.proyecto_app;

import android.content.Intent;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ciisa.jonathan.ahumada.proyecto_app.controller.AuthController;
import ciisa.jonathan.ahumada.proyecto_app.models.User;
import ciisa.jonathan.ahumada.proyecto_app.ui.DatePickerFragment;


public class RegisterActivity extends AppCompatActivity {

    private final String DATE_PATTERN = "yyyy-MM-dd";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);
    private TextInputLayout tilNameuser,tilBirthday, tilFirstname, tilLastName,tilStature,  tilPassword;
    private Button btnRegsiter, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        tilNameuser = findViewById(R.id.activity_register_field_name_user);
        tilFirstname = findViewById(R.id.activity_register_field_name);
        tilLastName = findViewById(R.id.activity_register_field_last_name);
        tilStature = findViewById(R.id.activity_register_field_stature);
        tilPassword = findViewById(R.id.activity_register_field_password);
        tilBirthday = findViewById(R.id.activity_register_field_birthday);

        btnRegsiter = findViewById(R.id.activity_register_btn_register);
        btnLogin = findViewById(R.id.activity_register_btn_login);


        btnRegsiter.setOnClickListener(view -> {

            String nameUser = tilNameuser.getEditText().getText().toString();
            String firstName = tilFirstname.getEditText().getText().toString();
            String lastName = tilLastName.getEditText().getText().toString();
            String stature = tilStature.getEditText().getText().toString();
            String password = tilPassword.getEditText().getText().toString();
            String birthday = tilBirthday.getEditText().getText().toString();



            boolean nameuserValid = !nameUser.isEmpty();
            boolean firstnameValid = !firstName.isEmpty();
            boolean lastNameValid = !lastName.isEmpty();
            boolean statureValid = !stature.isEmpty();
            boolean passwordValid = !password.isEmpty();
            boolean birthdayValid = !birthday.isEmpty();

            if (!nameuserValid) {
                tilNameuser.setError("Campo requerido");
            } else {
                tilNameuser.setError(null);
                tilNameuser.setErrorEnabled(false);
            }

            if (!firstnameValid) {
                tilFirstname.setError("Campo requerido");
            } else {
                tilFirstname.setError(null);
                tilFirstname.setErrorEnabled(false);
            }

            if (!lastNameValid) {
                tilLastName.setError("Campo requerido");
            } else {
                tilLastName.setError(null);
                tilLastName.setErrorEnabled(false);
            }

            if (!statureValid) {
                tilStature.setError("Campo requerido");
            } else {
                tilStature.setError(null);
                tilStature.setErrorEnabled(false);
            }

            if (!passwordValid) {
                tilPassword.setError("Campo requerido");
            } else {
                tilPassword.setError(null);
                tilPassword.setErrorEnabled(false);
            }

            if (!birthdayValid) {
                tilBirthday.setError("Campo requerido");
            } else {
                tilBirthday.setError(null);
                tilBirthday.setErrorEnabled(false);
            }

            if (nameuserValid && firstnameValid && lastNameValid && statureValid && passwordValid && birthdayValid) {
                Date birthdayDate = null;
                try {
                    birthdayDate = dateFormatter.parse(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                double statureDate = 0.0;
                try {
                    statureDate = Double.parseDouble(stature);
                    tilStature.setError(null);
                    tilStature.setErrorEnabled(false);
                } catch (NumberFormatException error){
                    tilStature.setError("Estatura es invalida");
                    return;
                }

                User user = new User(nameUser, firstName, lastName, birthdayDate, statureDate);
                user.setPassword(password);

                AuthController controller = new AuthController(view.getContext());
                controller.register(user);
            } else {
                Toast.makeText(view.getContext(), "Campos inválidos", Toast.LENGTH_SHORT).show();
            }


        });

        tilBirthday.getEditText().setOnClickListener(view -> {
            DatePickerFragment.showDatePickerDialog(this, tilBirthday, new Date());
        });

        btnLogin.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),LoginActivity.class);
            startActivity(i);
            finish();
        });


    }
}