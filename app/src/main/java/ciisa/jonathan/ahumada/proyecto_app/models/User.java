package ciisa.jonathan.ahumada.proyecto_app.models;

import java.util.Date;

public class User implements IUser{

    private long id;
    private String username;
    private String firstName;
    private String lastName;
    private Date birthday;
    private double height;
    private String password;

    public User(String username, String firstName, String lastName, Date birthday, double height) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.height = height;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public double getHeight() {
        return height;
    }

    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
