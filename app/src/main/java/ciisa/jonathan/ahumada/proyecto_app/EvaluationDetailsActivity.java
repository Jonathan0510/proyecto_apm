package ciisa.jonathan.ahumada.proyecto_app;

import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import ciisa.jonathan.ahumada.proyecto_app.controller.EvaluationController;
import ciisa.jonathan.ahumada.proyecto_app.models.Evaluation;

public class EvaluationDetailsActivity extends AppCompatActivity {
    private final String DATE_PATTERN = "yyyy-MM-dd";
    SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_PATTERN);

    private TextView tvIdEvaluation, tvDate, tvWeight, tvImc, tvIdUser;
    private Button btnReturn, btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation_details);

        Evaluation evaluation = (Evaluation) getIntent().getSerializableExtra("evaluation");

        tvIdUser = findViewById(R.id.activity_evaluation_detail_tv_id_user);
        tvIdEvaluation = findViewById(R.id.activity_evaluation_detail_tv_id_evaluation);
        tvDate = findViewById(R.id.activity_evaluation_detail_date);
        tvWeight = findViewById(R.id.activity_evaluation_detail_weight);
        tvImc = findViewById(R.id.activity_evaluation_detail_imc);

        btnReturn = findViewById(R.id.activity_evaluation_detail_btn_return);
        btnDelete = findViewById(R.id.activity_evaluation_detail_btn_delete);

        String birthdaytoString = dateFormatter.format(evaluation.getBirthday());

        tvIdUser.setText(Long.toString(evaluation.getUserId()));
        tvIdEvaluation.setText(Long.toString(evaluation.getId()));
        tvDate.setText(birthdaytoString);
        tvWeight.setText(Double.toString(evaluation.getWeight()));
        tvImc.setText(Double.toString(evaluation.getImc()));

        btnReturn.setOnClickListener(view -> {
            Intent i = new Intent (view.getContext(),SearchEvaluationsActivity.class);
            startActivity(i);
            finish();
        });

        btnDelete.setOnClickListener(view -> {
            EvaluationController controller = new EvaluationController(view.getContext());
            controller.deleteEvaluation(evaluation.getId());
        });
    }


}