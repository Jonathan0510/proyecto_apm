package ciisa.jonathan.ahumada.proyecto_app.models;

import java.util.Date;

public interface IEvaluation {

     long getId();
     Date getBirthday();
     double getWeight();
     double getImc();
     long getUserId();


}
