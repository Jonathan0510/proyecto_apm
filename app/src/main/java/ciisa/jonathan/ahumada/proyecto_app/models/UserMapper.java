package ciisa.jonathan.ahumada.proyecto_app.models;

public class UserMapper {
    private IUser user;

    public UserMapper(IUser user) {
        this.user = user;
    }

    public UserEntity toEntity(){
        return new UserEntity(
                this.user.getId(),
                this.user.getUsername(),
                this.user.getFirstName(),
                this.user.getLastName(),
                this.user.getBirthday(),
                this.user.getHeight(),
                this.user.getPassword()
        );
    }

    public User toBase(){
        User user = new User(
                this.user.getUsername(),
                this.user.getFirstName(),
                this.user.getLastName(),
                this.user.getBirthday(),
                this.user.getHeight()
                );
        user.setPassword(this.user.getPassword());
        user.setId(this.user.getId());
        return user;
    }
}
