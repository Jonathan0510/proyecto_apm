package ciisa.jonathan.ahumada.proyecto_app.lib;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import ciisa.jonathan.ahumada.proyecto_app.dao.EvaluationDao;
import ciisa.jonathan.ahumada.proyecto_app.dao.UserDao;
import ciisa.jonathan.ahumada.proyecto_app.models.EvaluationEnity;
import ciisa.jonathan.ahumada.proyecto_app.models.UserEntity;
import ciisa.jonathan.ahumada.proyecto_app.utils.Converters;

@Database(entities = {UserEntity.class, EvaluationEnity.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class databaseConnectors extends RoomDatabase {
    private static final String DB_NAME = "imc_app_db";
    private static databaseConnectors instance;

    public static synchronized databaseConnectors getInstance(Context ctx){
        if (instance == null) {
            instance = Room.databaseBuilder(ctx.getApplicationContext(), databaseConnectors.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract UserDao userDao();
    public abstract EvaluationDao evaluationDao();
}
